import React, { useState, useEffect } from "react";
import { Container, Form, Table, Button } from "react-bootstrap";
import { deleteCategory, fetchCategory, postCategory, updateCategory } from "../services/category_service";

const Category = () => {

  const [category, setCategory] = useState([])
  const [isUpdate, setIsUpdate] = useState(null)
  const [newCategory, setNewCategory] = useState(null)

  useEffect(() => {
    fetch();
  }, [])
  const onEdit = (id, name) => {
    setIsUpdate(id);
    setNewCategory(name)
    document.getElementById("text").value = name

  }

  const onUpdate = async (e) => {
    e.preventDefault();
    let category1 = { name: newCategory }
    updateCategory(isUpdate, category1).then(() => fetch())
    setIsUpdate(null)
    document.getElementById("text").value = "";
  }
  const fetch = async () => {
    let category = await fetchCategory();
    setCategory(category);
  }

  const onAdd = async (e) => {
    e.preventDefault();
    let category1 = { name: newCategory }
    postCategory(category1).then(() => fetch())
    document.getElementById('text').value = "";
  }

  function onDelete(id) {
    deleteCategory(id)
    let afterDel = category.filter((item) => item._id !== id)
    setCategory(afterDel)
  }
  return (
    <Container>
      <h1 className="my-3">Category</h1>
      <Form>
        <Form.Group controlId="category">
          <Form.Label>Category Name</Form.Label>
          <Form.Control type="text" placeholder="Category Name" />
          <br />
          <Button variant="secondary">Add</Button>
          <Form.Text className="text-muted"></Form.Text>
        </Form.Group>
      </Form>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {
            category.map((item, index) => (
              <tr key={index}>
                <td>{index + 1}</td>
                <td>{index.name}</td>
                <td>
                  <Button size='sm' variant='warning' onClick={() => onEdit(item._id, item.name)}>Edit</Button>
                  {" "}
                  <Button size="sm" variant="danger" onClick={() => onDelete(item._id)}>Delete</Button>
                </td>
              </tr>
            ))
          }
        </tbody>
      </Table>
    </Container>
  );
};

export default Category;
