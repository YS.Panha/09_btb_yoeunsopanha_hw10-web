import React, { useState, useEffect } from 'react'
import { Container } from 'react-bootstrap'
import { useParams } from 'react-router'
import { fetchArticleById } from '../services/article_service'

function ViewArticle() {

  const [article, setArticle] = useState()
  const { id } = useParams()

  useEffect(() => {
    if (id) {
      fetchArticleById(id).then(article => {
        setArticle(article)
      })
    }
  }, [])

  return (
    <Container>
      {article ? <>
        <h1>{article.title}</h1>
        <p>{article.description}</p>
      </> :
        ''}
    </Container>
  )
}

export default ViewArticle
