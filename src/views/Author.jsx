import React, { useEffect, useState } from "react";
import { Container, Form, Row, Col, Button, Table } from "react-bootstrap";
import { deleteAuthor, fetchAuthor, postAuthor, updateAuthor, } from "../services/author_service";
import { uploadImage } from "../services/article_service";

function Author() {
  const [imageURL, setImageURL] = useState("https://designshack.net/wp-content/uploads/placeholder-image.png");
  const [imageFile, setImageFile] = useState(null);
  const [author, setAuthor] = useState([]);
  const [name, setName] = useState(null);
  const [email, setEmail] = useState(null);
  const [isUpdate, setIsUpdate] = useState(null);

  useEffect(() => {
    fetch();
  }, []);

  const fetch = async () => {
    let Author = await fetchAuthor();
    setAuthor(Author);
  };

  const onAdd = async (e) => {
    e.preventDefault();
    let Author = { name, email };

    if (imageFile) {
      let url = await uploadImage(imageFile);
      Author.image = url;
    }
    postAuthor(Author).then((message) => {
      alert(message);
      fetch();
    });
    setImageFile(null);
    clear();
  };

  function clear() {
    document.getElementById("name").value = "";
    document.getElementById("email").value = "";
    document.getElementById("img").value = "";
    document.getElementById("image").src =
      "https://designshack.net/wp-content/uploads/placeholder-image.png";
  }

  function onDelete(id) {
    deleteAuthor(id);
    let afterDel = author.filter((item) => item._id !== id);
    setAuthor(afterDel);
  }

  const onEdit = (id, name, email, image) => {
    setIsUpdate(id)
    setName(name)
    setEmail(email)
    document.getElementById("name").value = name;
    document.getElementById("email").value = email;
    document.getElementById("image").src = image;
  };

  const onUpdate = async (e) => {
    e.preventDefault();
    let newAuthor = { name, email };

    if (imageFile) {
      let url = await uploadImage(imageFile);
      newAuthor.image = url;
    }

    updateAuthor(isUpdate, newAuthor).then(() => fetch());
    setIsUpdate(null);
    setImageFile(null);
    clear();
  };

  return (
    <div>
      <Container>
        <h1 className="my-3">Author</h1>
        <Row>
          <Col md={8}>
            <Form>
              <Form.Group>
                <Form.Label>Author Name</Form.Label>
                <Form.Control
                  type="text"
                  id="name"
                  placeholder="Author Name"
                  onChange={(e) => setName(e.target.value)}
                />
                <Form.Text className="text-muted"></Form.Text>
              </Form.Group>

              <Form.Group>
                <Form.Label>Email</Form.Label>
                <Form.Control
                  type="text"
                  id="email"
                  placeholder="Email"
                  onChange={(e) => setEmail(e.target.value)}
                ></Form.Control>
              </Form.Group>
              <Button
                variant="primary"
                type="submit"
                onClick={isUpdate !== null ? onUpdate : onAdd}
              >
                {isUpdate !== null ? "Update" : "Add"}
              </Button>
            </Form>
          </Col>
          <Col md={4}>
            <img id="image" className="w-75" src={imageURL} />
            <Form>
              <Form.Group>
                <Form.File
                  id="img"
                  label="Choose Image"
                  onChange={(e) => {
                    let url = URL.createObjectURL(e.target.files[0]);
                    setImageFile(e.target.files[0]);
                    setImageURL(url);
                  }}
                />
              </Form.Group>
            </Form>
          </Col>
        </Row>

        <Table striped bordered hover>
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Name</th>
              <th scope="col">Email</th>
              <th scope="col">Image</th>
              <th scope="col">Action</th>

            </tr>
          </thead>
          <tbody>
            {author.map((item, index) => (
              <tr key={index}>
                <td>{index + 1}</td>
                <td>{item.name}</td>
                <td>{item.email}</td>
                <td>
                  <img src={item.image} alt="" width={200} height={120} />
                </td>
                <td>
                  <Button
                    size="sm"
                    variant="warning"
                    onClick={() =>
                      onEdit(item._id, item.name, item.email, item.image)
                    }
                  >
                    Edit
                  </Button>{" "}
                  <Button
                    size="sm"
                    variant="danger"
                    onClick={() => onDelete(item._id)}
                  >
                    Delete
                  </Button>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      </Container>
    </div>
  );
}
export default Author